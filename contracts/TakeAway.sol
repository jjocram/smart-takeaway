//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";

contract TakeAway {
    enum ItemType{ SANDWICH, FRIED, DRINK }
    struct Item {
        ItemType itemType;
        string name;
        uint price;
    }
    struct Bill {
        Item[] items;
        bool payed;
    }

    bytes32[] public billsList;
    mapping(bytes32 => Bill) public billStructs;

    address payable takeAwayOwner;

    constructor(address payable _takeAwayOwner) {
        console.log("Deploying the TakeAway owned by: ", _takeAwayOwner);
        takeAwayOwner = _takeAwayOwner;
    }

   function newBill(bytes32 key, Item[] memory items) public {
       billsList.push(key);
       for (uint i=0; i < items.length; i++){
            billStructs[key].items.push(items[i]);
        }
   }

    function addItem(bytes32 billKey, ItemType _itemType, string calldata _name, uint _price) public returns(bool success) {
        Item memory newItem = Item(_itemType, _name, _price);
        billStructs[billKey].items.push(newItem);
        return true;
    }

    function payBill(bytes32 billKey) public payable{
        require(billStructs[billKey].items.length > 0, "The bill is empty");

        uint256 total = 0;
        uint number_of_sandwich = 0;
        uint total_only_sandwich_and_fried = 0;
        for (uint i=0; i < billStructs[billKey].items.length; i++) {
            total = total + billStructs[billKey].items[i].price;

            if (billStructs[billKey].items[i].itemType == ItemType.SANDWICH || billStructs[billKey].items[i].itemType == ItemType.FRIED) {
                total_only_sandwich_and_fried = total_only_sandwich_and_fried + billStructs[billKey].items[i].price;
            }

            if (billStructs[billKey].items[i].itemType == ItemType.SANDWICH) {
                number_of_sandwich++;
            }
        }

        if (total_only_sandwich_and_fried > 50) {
            total = total - (total / 10);
        }

        if (number_of_sandwich > 5) {
            uint price_sandiwch_less_expensive = 0;
            bool keepLookingForASandwich = true;
            //find first sandwich
            for(uint i = 0; keepLookingForASandwich; i++){
                if (billStructs[billKey].items[i].itemType == ItemType.SANDWICH) {
                    keepLookingForASandwich = false;
                    price_sandiwch_less_expensive = billStructs[billKey].items[i].price;
                }
            }

            for (uint i=0; i < billStructs[billKey].items.length; i++) {
                if (billStructs[billKey].items[i].itemType == ItemType.SANDWICH && billStructs[billKey].items[i].price < price_sandiwch_less_expensive) {
                    price_sandiwch_less_expensive = billStructs[billKey].items[i].price;
                }
            }

            total = total - (price_sandiwch_less_expensive/2);
        }

        if (total <= 10 && total > 0) {
            total = total + 5;
        }

        takeAwayOwner.transfer(total);
        billStructs[billKey].payed = true;
    }

    function getBill(bytes32 billKey) public view returns(Bill memory) {
        return billStructs[billKey];
    }

}
