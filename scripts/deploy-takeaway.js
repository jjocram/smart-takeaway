const hre = require("hardhat");

async function main() {
    const TakeAwayContract = await hre.ethers.getContractFactory("TakeAway");
    const takeAway = await TakeAwayContract.deploy("0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266");
    await takeAway.deployed();

  console.log("TakeAway deployed to:", takeAway.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
