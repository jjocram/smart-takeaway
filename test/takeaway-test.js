const { expect } = require("chai");
const { ethers } = require("hardhat");

function randomBytes32 (length = 64){
  let chars = "12345678890abcdef";
  let str = '0x';
  for (let i =0; i < length; i++) {
    str += chars.charAt(Math.floor(Math.random() * chars.length));
  }
  return str;
};

const spring_sandwich = {
  itemType: 0,
  name: "Spring Sandwich",
  price: 5
};

const vegan_sandwich = {
  itemType: 0,
  name: "Vegan Sandwich",
  price: 6
}

const ascoli_olives = {
  itemType: 1,
  name: "Ascoli Olives",
  price: 3
}

const arancino = {
  itemType: 1,
  name: "Arancino",
  price: 3
}

const coca_cola = {
  itemType: 2,
  name: "CocaCola",
  price: 2
}

const natural_water = {
  itemType: 2,
  name: "Natural Water",
  price: 1
}




describe("TakeAway", function () {
  let TakeAwayContract;
  let takeAway;

  this.beforeAll(async () => {
    TakeAwayContract = await ethers.getContractFactory("TakeAway");
    takeAway = await TakeAwayContract.deploy("0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266");
    await takeAway.deployed();
  });

  it("Should create a new bill with a new item", async () => {
    const billKey = randomBytes32();
    const newBillResult = await takeAway.newBill(billKey, [spring_sandwich]);
    newBillResult.wait();

    const resultAfterItemAdd = await takeAway.getBill(billKey);
    expect(resultAfterItemAdd.items[0].name).to.equal(spring_sandwich.name);
    expect(resultAfterItemAdd.items[0].price).to.equal(spring_sandwich.price);
  });

  it("Should fail because the initial array of items is empty", async () => {
    const billKey = randomBytes32();
    const newBillResult = await takeAway.newBill(billKey, []);
    newBillResult.wait();

    await expect(takeAway.payBill(billKey)).to.be.revertedWith("The bill is empty");
  });

  it("Should fail because no ethers is sent", async () => {
    const billKey = randomBytes32();
    const newBillResult = await takeAway.newBill(billKey, [vegan_sandwich]);
    newBillResult.wait();

    await expect(takeAway.payBill(billKey)).to.be.reverted;
  });

  it("Should fail because not enough ethers is sent", async () => {
    const billKey = randomBytes32();
    const newBillResult = await takeAway.newBill(billKey, [vegan_sandwich]);
    newBillResult.wait();

    await expect(takeAway.payBill(billKey, {value: 1})).to.be.reverted;
  });

  it("Should create a new bill with a new item and then add another item", async () => {
    const billKey = randomBytes32();
    const newBillResult = await takeAway.newBill(billKey, [spring_sandwich]);
    newBillResult.wait();

    await takeAway.addItem(billKey, vegan_sandwich.itemType, vegan_sandwich.name, vegan_sandwich.price);

    const resultAfterItemAdd = await takeAway.getBill(billKey);
    expect(resultAfterItemAdd.items[0].name).to.equal(spring_sandwich.name);
    expect(resultAfterItemAdd.items[0].price).to.equal(spring_sandwich.price);
    expect(resultAfterItemAdd.items[1].name).to.equal(vegan_sandwich.name);
    expect(resultAfterItemAdd.items[1].price).to.equal(vegan_sandwich.price);
  });

  it("Should pay a bill without any modifiers", async () => {
    const billKey = randomBytes32();
    const newBillResult = await takeAway.newBill(billKey, [spring_sandwich, vegan_sandwich]);
    newBillResult.wait();

    const [owner, customer] = await ethers.getSigners();

    const total = spring_sandwich.price + vegan_sandwich.price;

    const transaction = await takeAway.connect(customer).payBill(billKey, {value: total});
    await expect(transaction).to.changeEtherBalance(customer, -total);
    await expect(transaction).to.changeEtherBalance(owner, +total);

    const resultAfterPay = await takeAway.getBill(billKey);
    expect(resultAfterPay.payed).to.equal(true);
  });

  it("Should pay a bill with the deal: buy more than 5 sandiwich and the less expansive is payed the half", async () => {
    const [owner, customer] = await ethers.getSigners();
    const billItems = [vegan_sandwich, vegan_sandwich, vegan_sandwich, vegan_sandwich, spring_sandwich, spring_sandwich];
    const totalExpected = 32; // (6*4 + 5*2) - (5//2)
    const billKey =  randomBytes32();

    const newBillResult = await takeAway.newBill(billKey, billItems);
    newBillResult.wait();

    const transaction = await takeAway.connect(customer).payBill(billKey, {value: totalExpected});
    await expect(transaction).to.changeEtherBalance(customer, -totalExpected);
    await expect(transaction).to.changeEtherBalance(owner, +totalExpected);

    const resultAfterPay = await takeAway.getBill(billKey);
    expect(resultAfterPay.payed).to.equal(true);
  });

  it("Should pay a bill without sandiwiches", async () => {
    const [owner, customer] = await ethers.getSigners();
    const billItems = [arancino, ascoli_olives, coca_cola, natural_water, natural_water, natural_water];
    const totalExpected = 11; // 3 + 3 + 2 + 1*3
    const billKey =  randomBytes32();

    const newBillResult = await takeAway.newBill(billKey, billItems);
    newBillResult.wait();

    const transaction = await takeAway.connect(customer).payBill(billKey, {value: totalExpected});
    await expect(transaction).to.changeEtherBalance(customer, -totalExpected);
    await expect(transaction).to.changeEtherBalance(owner, +totalExpected);

    const resultAfterPay = await takeAway.getBill(billKey);
    expect(resultAfterPay.payed).to.equal(true); 
  });

  it("Should pay a bill which costs more than 50 so with the sale of 10% off", async () => {
    const [owner, customer] = await ethers.getSigners();
    const billItems = [vegan_sandwich, vegan_sandwich, vegan_sandwich, vegan_sandwich, vegan_sandwich, arancino, arancino, arancino, arancino, arancino, ascoli_olives, ascoli_olives, ascoli_olives, ascoli_olives, ascoli_olives];
    const totalExpected = 54; // (6*5 + 3*5 + 3*5) - (10% of 60)
    const billKey =  randomBytes32();

    const newBillResult = await takeAway.newBill(billKey, billItems);
    newBillResult.wait();

    const transaction = await takeAway.connect(customer).payBill(billKey, {value: totalExpected});
    await expect(transaction).to.changeEtherBalance(customer, -totalExpected);
    await expect(transaction).to.changeEtherBalance(owner, +totalExpected);

    const resultAfterPay = await takeAway.getBill(billKey);
    expect(resultAfterPay.payed).to.equal(true); 
  });

  it("Should pay a bill which costs more than 50 so with the sale of 10% off and has more than 5 sandiwiches so the less expansive costs the half", async () => {
    const [owner, customer] = await ethers.getSigners();
    const billItems = [vegan_sandwich, vegan_sandwich, vegan_sandwich, vegan_sandwich, vegan_sandwich, arancino, arancino, arancino, arancino, arancino, ascoli_olives, ascoli_olives, ascoli_olives, ascoli_olives, ascoli_olives, spring_sandwich, spring_sandwich];
    const totalExpected = 61; // (6*5 + 3*5 + 3*5 + 5*2) - (10% of 70) - (5//2)
    const billKey =  randomBytes32();

    const newBillResult = await takeAway.newBill(billKey, billItems);
    newBillResult.wait();

    const transaction = await takeAway.connect(customer).payBill(billKey, {value: totalExpected});
    await expect(transaction).to.changeEtherBalance(customer, -totalExpected);
    await expect(transaction).to.changeEtherBalance(owner, +totalExpected);

    const resultAfterPay = await takeAway.getBill(billKey);
    expect(resultAfterPay.payed).to.equal(true); 
  });

    it("Should pay a bill with a supplement of 5 because its price is less than 10", async () => {
    const [owner, customer] = await ethers.getSigners();
    const billItems = [vegan_sandwich];
    const totalExpected = 11; // 6 + 5
    const billKey =  randomBytes32();

    const newBillResult = await takeAway.newBill(billKey, billItems);
    newBillResult.wait();

    const transaction = await takeAway.connect(customer).payBill(billKey, {value: totalExpected});
    await expect(transaction).to.changeEtherBalance(customer, -totalExpected);
    await expect(transaction).to.changeEtherBalance(owner, +totalExpected);

    const resultAfterPay = await takeAway.getBill(billKey);
    expect(resultAfterPay.payed).to.equal(true); 
  });

});
