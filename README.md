# Smart Takeaway

This repository contains the source code for the demo showed in the HardHat seminar that was held by Marco Ferrati for the course "Blockchain and cryptocurrencies" in the A.Y. 2021/22 at the Alma Mater Studiorum University of Bologna.

[![coverage report](https://gitlab.com/jjocram/smart-takeaway/badges/main/coverage.svg)](https://gitlab.com/jjocram/smart-takeaway/-/commits/main)

[Code coverage report](https://jjocram.gitlab.io/smart-takeaway/)
