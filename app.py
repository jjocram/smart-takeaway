from web3 import Web3
from sys import exit
from json import load
from random import choice

CHARS_FOR_KEY = list("1234567890abcdef")
NETWORK_ADDRESS = "http://localhost:8545"
TAKEAWAY_CONTRACT_ABI = load(open("./artifacts/contracts/TakeAway.sol/TakeAway.json"))['abi']
TAKEAWAY_CONTRACT_ADDRESS = input("Takeaway Contract address:")

web3_client = Web3(Web3.HTTPProvider(NETWORK_ADDRESS))
if not web3_client.isConnected():
    print(f"Cannot connect to the ethereum network at: {NETWORK_ADDRESS}")
    exit(1)

takeaway_contract = web3_client.eth.contract(address=Web3.toChecksumAddress(TAKEAWAY_CONTRACT_ADDRESS),
                                             abi=TAKEAWAY_CONTRACT_ABI)

print("""Which product do you want to buy:
    [1] Toast
    [2] Chips
    [3] Sprite""")
product_id = input("Product number:")
product = {}
if product_id == "1":
    product = {
        "itemType": 0,
        "name": "Toast with cheese and ham",
        "price": 50
    }
elif product_id == "2":
    product = {
        "itemType": 1,
        "name": "Chips with ketchup",
        "price": 12
    }
else:
    product = {
        "itemType": 2,
        "name": "Sprite",
        "price": 5
    }

bill_key = "0x"+"".join(choice(list("1234567890abcdef")) for _ in range(64))

transaction_hash = takeaway_contract.functions.newBill(bill_key, [product]).transact()
transaction_recepit = web3_client.eth.waitForTransactionReceipt(transaction_hash)

bill = takeaway_contract.functions.getBill(bill_key).call()
print(f"""
------------------------
TakeAway bill:
------------------------
product:
    - {bill[0][0][1]} -------------- {bill[0][0][2]} wei
------------------------
payed: {bill[1]}
------------------------""")


